FROM sammobach/go:latest as build
LABEL maintainer="Sam Mobach <sam@pleio.nl>"
RUN mkdir /app
ADD . /app/
WORKDIR /app
RUN mkdir -p /app/bin && go build -o /app/bin/otel-server-demo .
CMD ["/app/bin/otel-server-demo"]

FROM alpine:3.18
LABEL maintainer="Sam Mobach <sam@pleio.nl>"
RUN mkdir /app
RUN adduser -S pleio -D -u 10000 -s /bin/nologin
COPY --from=build /app /app
WORKDIR /app
USER 10000
CMD ["/app/bin/otel-server-demo"]
